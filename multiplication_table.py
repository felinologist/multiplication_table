# importing the randint method from random module

from random import randint

# introducing the wins and fails variables and setting their values to zero

wins = 0
fails = 0

# creating the main logic

while fails < 3 or wins < 10:
    x = randint(2, 9)
    y = randint(2, 9)
    answer = int(input("What is " + str(x) + " * " + str(y) + " ? "))
    result = x * y
    if answer == result:
        wins += 1
        if wins < 10:
            print("Well done!")
        else:
            if fails == 0:
                print("You won! Perfect!")
            else:
                print("You won!")
            break
    else:
        fails += 1
        if fails < 3:
            print("Try again!")
        else:
            print("Game over!")
            break
